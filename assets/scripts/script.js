//Arrays
// used to store multiple values in a single variable
// array=[]

// let randomThings = [23, "Albert", true];

// console.log(randomThings);

// console.log(students[3]);

// index
// .length
// console.log(students.length);

// let password = "iloveyou";
// console.log(password.length)
// if(password.length < 9){
// 	console.log("Invalid Password");
// }

// add a value to an array
// .push

// student.push("Krystel");

let students = ['Darwin', 'Rancel', 'Yong', 'Kaka'];
// to remove the last value in an array and return the new array
// .pop()
// to remove an element based on its index;
// and to add elements using the index as its position.
// .splice(index, # of element we want to remove, element we want to add)

//Iteration Method
// .forEach

students.forEach(function(student, index){
	if(index===0 || index===3){
		console.log("Contestant #"+(index+1)+" "+student+ " pogi");
	}else{
		console.log("Thank you for joining "+student);
	}
	
})