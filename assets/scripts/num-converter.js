let singleDigit = ["Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"];


let teens = ["", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"];
let tens = ["", "ten", "twenty", "thirty", "fourty", "fifty", "sixty", "seventy", "eighty", "ninety"];
function numConverter(inputNo){
	if (inputNo < 10 && inputNo >= 0){
		return singleDigit[inputNo]
	}
	if(inputNo<99 && inputNo % 10 === 0){
		return tens[inputNo/10]
	}
	if(inputNo<20 && inputNo>10){
		return teens[inputNo-10]
	}

	if(inputNo>10 && inputNo<100){
		let singleValue = inputNo % 10;
		let tensValue = (inputNo - singleValue)/10;
		return tens[tensValue] + " " + singleDigit[singleValue]
	}
	if(inputNo>=100 && inputNo < 1000){
		let singleValue = (inputNo % 100)%10;
		let tensValue = ((inputNo - singleValue)%100)/10;
		let hundredValue = (inputNo - ((tensValue*10)+singleValue))/100;
		if(tensValue == 1){
			return singleDigit[hundredValue]+" hundred "+teens[singleValue];
		}else if(tensValue == 0 && singleValue == 0){
			return singleDigit[hundredValue]+" hundred";	
		}else if(tensValue == 0 && singleValue !== 0){
			return singleDigit[hundredValue]+" hundred and "+singleDigit[singleValue]
		}else if(tensValue !== 0 && singleValue === 0){
			return singleDigit[hundredValue]+" hundred and "+tens[tensValue]
		}else{
			return singleDigit[hundredValue]+" hundred and "+tens[tensValue]+" "+singleDigit[singleValue]
	
		}
	}


}